#include "Bezier.h"
#include "Line.h"
#include <iostream>

using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	int X[100];
	int Y[100];
	double t = 0;

	for (int i = 0; i < 100; i++) 
	{
		t += 0.01;
		X[i] = (int)((1 - t)*(1 - t)*p1.x + 2 * (1 - t)*t*p2.x + t * t*p3.x);
		Y[i] = (int)((1 - t)*(1 - t)*p1.y + 2 * (1 - t)*t*p2.y + t * t*p3.y);
	}
	for (int i = 0; i < 99; i++) {
		Bresenham_Line(X[i], Y[i], X[i + 1], Y[i + 1], ren);
	}

}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	int X[100];
	int Y[100];
	double t = 0;
	for (int i = 0; i < 100; i++) 
	{
		t += 0.01;
		X[i] = (int)((1 - t)*(1 - t)*(1 - t)*p1.x + 3 * (1 - t)*(1 - t)*t*p2.x + 3 * (1 - t)*t*t*p3.x + t * t*t*p4.x);
		Y[i] = (int)((1 - t)*(1 - t)*(1 - t)*p1.y + 3 * (1 - t)*(1 - t)*t*p2.y + 3 * (1 - t)*t*t*p3.y + t * t*t*p4.y);
	}
	for (int i = 0; i < 99; i++) {
		Bresenham_Line(X[i], Y[i], X[i + 1], Y[i + 1], ren);
	}

}
