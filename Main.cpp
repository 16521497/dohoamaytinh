#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;

bool isInRec(int x, int y, SDL_Rect *rect)
{
	bool inside = true;
	if ((x < rect->x) || (x > rect->x + rect->w) || (y < rect->y) || (y > rect->y + rect->h))
		return inside = false;
	return inside;
}

int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);

	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);

	SDL_RenderDrawRect(ren, rect1);
	SDL_RenderDrawRect(ren, rect2);
	SDL_RenderDrawRect(ren, rect3);
	SDL_RenderDrawRect(ren, rect4);
	SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);

	DrawCurve3(ren, p1, p2, p3, p4);

	SDL_RenderPresent(ren);
	SDL_Delay(1000);

	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	bool clicking = true;
	Vector2D *temp = new Vector2D;
	SDL_Rect *RectTemp = new SDL_Rect;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				int x, y;
				SDL_GetMouseState(&x, &y);

				if (isInRec(x, y, rect1)) {
					temp = &p1;
					RectTemp = rect1;
					clicking = true;
				}
				if (isInRec(x, y, rect2)) {
					temp = &p2;
					RectTemp = rect2;
					clicking = true;
				}
				if (isInRec(x, y, rect3)) {
					temp = &p3;
					RectTemp = rect3;
					clicking = true;
				}if (isInRec(x, y, rect4)) {
					temp = &p4;
					RectTemp = rect4;
					clicking = true;
				}

			}
			if (event.type == SDL_MOUSEBUTTONUP && clicking)
			{
				int x, y;
				SDL_GetMouseState(&x, &y);
				clicking = false;

				temp->x = x;
				temp->y = y;

				RectTemp->x = x - 3;
				RectTemp->y = y - 3;

				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
		
				SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };
				SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);

				SDL_RenderDrawRect(ren, rect1);
				SDL_RenderDrawRect(ren, rect2);
				SDL_RenderDrawRect(ren, rect3);
				SDL_RenderDrawRect(ren, rect4);
				SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);

				DrawCurve3(ren, p1, p2, p3, p4);
				SDL_RenderPresent(ren);
			}
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}